const mongoose = require('mongoose');

const TaskSchema = new mongoose.Schema({
    title: String,
    content: String
});

// LE DIGO A MONGO QUE ME CREE UN MODELO CON EL ESQUEMA DE ARRIBA
const TaskModel = mongoose.model('TaskModel', TaskSchema);

module.exports = TaskModel;
