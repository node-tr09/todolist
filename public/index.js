let btn = 0;
let boton = document.querySelector('#boton');

let nuevaTask = document.querySelector('#tarea');
let nuevoContent = document.querySelector('#contenido');
let listaUl = document.querySelector('#lista');

boton.addEventListener('click', function() {
    if (btn === 0 && nuevaTask.value !== '') {
        boton.value = "Añadir";
        addTask();
        nuevaTask.value = '';
        nuevoContent.value = '';
    }
    if (btn === 1) {
        boton.value = "Modificar";
        modifyTask();
    }
})



// API fetch GET
function getTask() {
    
    let llamada = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json'
        }
    };
    
    fetch('http://localhost:3000/tasks', llamada)
        .then(response => {
            return response.json();
        })
        .then(data => {

            while (listaUl.firstChild) {
                listaUl.removeChild(listaUl.firstChild)
            }
            
            for (i in data) {
                let newTask = document.createElement('li');
                newTask.id = data[i]._id;

                let newTitle = document.createElement('h3');
                newTitle.id = 't' + i;

                let newContent = document.createElement('p');
                newContent.id = 'c' + i;

                let btnModify = document.createElement('button');
                btnModify.textContent = 'Modificar';
                btnModify.id = 'm' + i;
                btnModify.className = 'modificar';
                btnModify.addEventListener('click', modificarTarea);

                let btnDelete = document.createElement('button');
                btnDelete.textContent = 'Borrar';
                btnDelete.id = 'b' + i;
                btnDelete.className = 'borrar';
                btnDelete.addEventListener('click', borrarTarea);

                newTitle.textContent = data[i].title;
                newContent.textContent = data[i].content;
                listaUl.appendChild(newTask);
                newTask.appendChild(newTitle);
                newTask.append(newContent);
                newTask.append(btnModify);
                newTask.append(btnDelete);
            }

        })
        .catch(function(err) {
            console.error(err);
        });
          
};
getTask();


// AGREGAR TAREA

function addTask() {


    let insertar = {
        method: 'POST',
        headers:{
            'Content-Type' : 'application/json',
            'Accept': 'application/json'
        },
        body:JSON.stringify({
            "title" : nuevaTask.value,
            "content" : nuevoContent.value
        })
    };
    
    fetch('http://localhost:3000/tasks', insertar)
        .then(response => getTask())
        .catch(error => console.error(error));

}


// MODIFICAR TAREA

let listaA;

function modificarTarea(e) {
    btn = 1;
    console.log(btn)
    boton.value = "Modificar";

    let campoUno = document.querySelector('#tarea');
    let campoDos = document.querySelector('#contenido');

    let titleModificar = e.target.parentNode.childNodes[0].textContent;
    let contentModificar = e.target.parentNode.childNodes[1].textContent;

    campoUno.value = titleModificar;
    campoDos.value = contentModificar;

    listaA = e.target.parentNode;
    
}

function modifyTask() {

    let campoUno = document.querySelector('#tarea');
    let campoDos = document.querySelector('#contenido');

    let modify = {
        method: 'PUT',
        headers:{
            'Content-Type' : 'application/json'
        },
        body:JSON.stringify({
            "title" : campoUno.value,
            "content" : campoDos.value
        })
    };

    fetch('http://localhost:3000/tasks/' + listaA.id, modify)
        .then(response => response.json())
        .then(data => getTask())
        .catch(error => console.error(error));

        btn = 0;
        boton.value = "Añadir";
        nuevaTask.value = '';
        nuevoContent.value = '';
        

}


// BORRAR TAREA

function borrarTarea(e) {

    let listaB = e.target.parentNode;

    let deleteTask = {
        method: 'DELETE',
        headers:{
            'Content-Type' : 'application/json'
        }
    };
    
    fetch('http://localhost:3000/tasks/' + listaB.id, deleteTask)
        .then(response => response.json())
        .then(data => getTask())
        .catch(error => console.error(error));
}

    



