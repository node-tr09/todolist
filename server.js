const express = require('express');
const app = express();
let bodyParser = require('body-parser');

const TaskModel = require('./models/TaskModel');

// CONECTAMOS A BBDD MONGO CON MONGOOSE
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/tasks', { useNewUrlParser: true });

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('¡Estamos conectados!');
})

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());


// GET
app.get('/tasks', (req, res) => {
    TaskModel.find((err, taskes) => {
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(taskes);
    });
});


// INSERT
app.post('/tasks', (req, res) => {
    
    const tareaUno = new TaskModel(req.body);

    tareaUno.save((err, storedTask) => {
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }
        return res.json(storedTask);
    });


});


// PUT
app.put('/tasks/:idenTask', (req, res) => {
    let obtener = req.params.idenTask;

    TaskModel.findById(obtener, function (err, findTask) {
        if (err) {
            return res.status(500).send({"error":"fallo"})
        }

        findTask.title = req.body.title;
        findTask.content = req.body.content;

        findTask.save((err, storedbook) => {
            if (err) {
                return res.status(500).send({"error":"fallo"})
            }
            return res.json(storedbook);
        });
    });
})


// DELETE
app.delete('/tasks/:idenTask', (req, res) => {
    let quitar = req.params.idenTask;
    TaskModel.deleteOne({ _id: quitar },(err, taskes) => {
            if (err) {
                return res.status(500).send({"error":"fallo"})
            }
            return res.json(taskes);
        });
})


// PARA ESCUCHAR EN EL PUERTO
app.listen(3000);